import spbAdmin from './api/SpbAdminApi';
class NotificationService {
	async getAllNotificationsSupp(payload) {
		return spbAdmin.post('spb/notification/getnotifbytenantsupp/', payload);
	}
	async getAllNotificationsPur(payload) {
		return spbAdmin.post('spb/notification/getnotifbytenantpur/', payload);
	}
	async getAllNotifications() {
		return spbAdmin.get('spb/notification/getnotifbytenant');
	}
	async getCompanyCurrentSupp(payload) {
		return spbAdmin.post('spb/notification/getcompanycurrentsupp/', payload);
	}
	async getCurrentSupplier(payload) {
		return spbAdmin.post('spb/notification/getsuppbindcomp/', payload);
	}
	async getPurNotifications(payload) {
		return spbAdmin.post('spb/notification/getnotifbypur', payload);
	}
	async getPurNotificationsSearch(payload) {
		return spbAdmin.post('spb/notification/getnotifbypursearch', payload);
	}
	async getNotifDetailPlanno(poNum) {
		const url = 'spb/notification/notifdetailplanno/' + poNum;
		// const url = 'spb/notification/notifdetailplanno', poNum;
		return spbAdmin.post(url);
	}
	async getNotifDetailPlannoRecipient(payload) {
        return spbAdmin.post('spb/notification/notifdetailplannorecipient/', payload);
    }
	async getNotifDetailPlannoSender(params) {
        const queryParams = params ? Object.keys(params).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k])).join('&') : '';
        // console.log('liat isi query params::', queryParams)
        return spbAdmin.post("spb/notification/notifdetailplannosender?" + queryParams);
    }
	async getNotifDetailPurchaseOrder(poNum) {
        return spbAdmin.post('spb/notification/notifdetailpurchaseorder/' + poNum);
    }
	async getCompanySupp(payload) {
		return spbAdmin.get(
			'/spb/usermapping/getusermappingsbyiduser/' + payload
		);
	}
}
export default new NotificationService();
