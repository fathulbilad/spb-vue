import spborder from "./api/SpbOrderApi";
import spbadmin from "./api/SpbAdminApi"
import spbKkt from './api/SpbKktApi';
class POFulfillment {
	async getDetail(keyId) {
		const response = await spborder.get(
			`/spb/po/detail/${keyId}`
		);
		return response.data[0];
	}

	async getItems(headerId, dbInstance) {
		const response = await spborder.get(
			`/spb/po/detail/${headerId}/items?dbinstance=${dbInstance}`
		);
		return response.data;
	}

	async getFileInfo(headerId) {
		const response = await spborder.get(
			`/spb/po/detail/attachments/${headerId}`
		);
		return response.data;
	}

	submitDataSupp(payloadkirim) {
		return spborder.post('/spb/po/submitdatasupp', payloadkirim)
	}

	async getCompanySupp(payload) {
		return spbadmin.get(
			'/spb/usermapping/getusermappingsbyiduser/' + payload
		);
	}
	getRecipientMail(data) {
		return spbadmin.post(
			'/spb/notification/getPlannoRecipientMailPur',
			data
		);
	}
	sendPONotifSupp(payload) {
		const url = `spb/notification/sendPOnotifsupp`;
		return spbadmin.post(url, payload);
	}
	sendPOEmail(data) {
		// const url = `adm/send/emailPlanno`;
		// return spbKkt.post(url, data);
		const queryParams = data ?
			Object.keys(data)
			.map(
				(k) =>
				encodeURIComponent(k) +
				'=' +
				encodeURIComponent(data[k])
			)
			.join('&') :
			'';
		return spbKkt.post('adm/send/emailPOFulfillToPurchasing?' + queryParams);
	}
	submitApi(payload) {
		return spborder.post('/spb/spb_pofulfillment/submitapi', payload);
	}
}

export default new POFulfillment();