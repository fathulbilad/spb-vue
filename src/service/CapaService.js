import spbAdmin from "./api/SpbAdminApi";
class CapaService {
    getAllCapa() {
        return spbAdmin.get('spb/notification/getallCapaList');
    }
    getTableNested() {
        return spbAdmin.get("/spb/notification/shipnotice");
      }

}
export default new CapaService();