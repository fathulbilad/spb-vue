import axios from "axios";

const spbReportApi = () => {
  const defaultOptions = {
    baseURL: "http://localhost:3015/",
  headers: {
    "Content-type": "application/json",
  },
};

  let instance = axios.create(defaultOptions);

  instance.interceptors.request.use(function (config) {
    // let token = localStorage.getItem('app_authKey');
    let token = sessionStorage.authKey;
    if (token != null){ 
      token = token.replaceAll('"', '');
    }
    config.headers.Authorization =  token ? `Bearer ${token}` : '';
    return config;
  });

  return instance;
};

export default spbReportApi();