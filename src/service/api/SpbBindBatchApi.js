import axios from "axios";

const spbBindBatchApi = () => {
  const defaultOptions = {
    baseURL: "http://localhost:3017/",
  headers: {
    "Content-type": "application/json",
  },
};


  let instance = axios.create(defaultOptions);

  instance.interceptors.request.use(function (config) {
    const token = sessionStorage.authKey;
    const tokenId = token.replaceAll('"', '');
    config.headers.Authorization =  tokenId ? `Bearer ${tokenId}` : '';
    return config;
  });

  return instance;
};

export default spbBindBatchApi();