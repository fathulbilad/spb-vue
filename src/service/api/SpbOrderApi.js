import axios from 'axios';

const spbOrderApi = () => {
	const defaultOptions = {
		baseURL: 'https://order.isupplier-portal.com/',
		// baseURL: 'http://localhost:3014/',

		headers: {
			'Content-type': 'application/json',
		},
	};

	let instance = axios.create(defaultOptions);

	instance.interceptors.request.use(function (config) {
		let token = sessionStorage.authKey;
		if (token != null) {
			token = token.replaceAll('"', '');
		}
		config.headers.Authorization = token ? `Bearer ${token}` : '';
		return config;
	});

	return instance;
};

export default spbOrderApi();
