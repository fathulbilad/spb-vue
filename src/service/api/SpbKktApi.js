import axios from 'axios';

const spbKktApi = () => {
	const defaultOptions = {
		baseURL: 'https://sync.isupplier-portal.com/',
		// baseURL: 'http://localhost:3000/',

		headers: {
			'Content-type': 'application/json',
		},
	};

	let instance = axios.create(defaultOptions);

	instance.interceptors.request.use(function (config) {
		let token = sessionStorage.authKey;
		if (token != null) {
			token = token.replaceAll('"', '');
		}
		config.headers.Authorization = token ? `Bearer ${token}` : '';
		return config;
	});

	return instance;
};

export default spbKktApi();

// import axios from 'axios';

// const spbKktApi = () => {
//   const defaultOptions = {
//     // baseURL: "https://sync.isupplier-portal.com/",
//     baseURL: "http://localhost:3000/",

//     headers: {
//     "Content-type": "application/json",
//     },
// };

//   let instance = axios.create(defaultOptions);

//   instance.interceptors.request.use(function (config) {

//     let token = localStorage.getItem('app_authKey');
//     if (token != null){
//       token = token.replaceAll('"', '');
//     }
//     config.headers.Authorization =  token ? `Bearer ${token}` : '';
//     return config;
//   });

//   return instance;
// };

// export default spbKktApi();
