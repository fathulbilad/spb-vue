import axios from 'axios';

const spbAdminApi = () => {
  const defaultOptions = {
    baseURL: "https://admin.isupplier-portal.com/",
    // baseURL: "http://localhost:3013/",

		headers: {
			'Content-type': 'application/json',
		},
	};

	let instance = axios.create(defaultOptions);

	instance.interceptors.request.use(function (config) {
		let token = sessionStorage.authKey;
		let tokenId = token;
		if (token != null) {
			tokenId = token.replaceAll('"', '');
		}
		config.headers.Authorization = tokenId ? `Bearer ${tokenId}` : '';
		return config;
	});

	return instance;
};

export default spbAdminApi();
