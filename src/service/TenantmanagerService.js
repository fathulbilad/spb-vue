import spbKkt from "./api/SpbKktApi";
class TenantmanagerService{
    retriveTenantByNamein(data){
        let obj = {payloads : data}
        return spbKkt.post('adm/tenants/getAllTenantsIn', obj);
    }
}
export default new TenantmanagerService();
