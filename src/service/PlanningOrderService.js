import spborder from './api/SpbOrderApi';
import spbadmin from './api/SpbAdminApi';
import spbKkt from './api/SpbKktApi';
class PlanningOrder {
	getTableSuppData;
	getTableData() {
		return spborder.get('/spb/spb_planningorder/tableopenreq');
	}
	getTableDataSupp(payload) {
		return spborder.post('/spb/spb_planningorder/tableopenreqsup', payload);
	}
	GetTableNested(params) {
		const queryParams = params
			? Object.keys(params)
					.map(
						(k) =>
							encodeURIComponent(k) +
							'=' +
							encodeURIComponent(params[k])
					)
					.join('&')
			: '';
		return spborder.post(
			'/spb/spb_planningorder/getnestedtable?' + queryParams
		);
	}
	getSearchTableNested(params) {
		const queryParams = params
			? Object.keys(params)
					.map(
						(k) =>
							encodeURIComponent(k) +
							'=' +
							encodeURIComponent(params[k])
					)
					.join('&')
			: '';
		return spborder.post(
			'/spb/spb_planningorder/searchnestedtable?' + queryParams
		);
	}
	GetPreparer(payload) {
		return spborder.post('/spb/spb_planningorder/getpreparer', payload);
	}
	GetSupplier() {
		return spborder.post('/spb/spb_planningorder/getsupplier');
	}
	getCompany() {
		return spborder.post('/spb/spb_planningorder/getcompany');
	}
	getCompanyId(id) {
		return spborder.post('/spb/spb_planningorder/getcompanysupp/', id);
	}
	getSearchDropdownCompany(payload) {
		return spborder.post('/spb/spb_planningorder/getsupplier', payload);
	}
	getSearchDropdownSupp(payload) {
		console.log(payload);
		return spborder.post('/spb/spb_planningorder/getsupsearch', payload);
	}
	getSearchDropdownComp(payload) {
		// console.log(payload)
		return spborder.post('/spb/spb_planningorder/getsupcomp', payload);
	}
	getPoNumber() {
		return spborder.post('/spb/spb_planningorder/getponumber');
	}
	getSearchDropdownPoNum(payload) {
		// console.log(payload)
		return spborder.post('/spb/spb_planningorder/getposearch', payload);
	}
	GetLovStatus() {
		return spborder.post('/spb/spb_planningorder/getstatus');
	}
	getTableSearch(data) {
		return spborder.post(`/spb/spb_planningorder/tableopenreq/` + data);
	}
	getGroup() {
		return spborder.post(`/spb/spb_planningorder/getrowgroup`);
	}
	getCompanySupp(payload) {
		return spbadmin.get(
			'/spb/usermapping/getusermappingsbyiduser/' + payload
		);
	}

	// INI GET PLAN ORDER UNTUK DASHBOARD CHARTS
	getMostOrderedItems(payload) {
		return spborder.get(
			`/spb/spb_planningorder/mostOrderedItems/${payload}`
		);
	}
	getMostOrderedSuppliers() {
		return spborder.get(`/spb/spb_planningorder/mostOrderedSuppliers`);
	}

	getMonthlyPlanningOrders(payload) {
		return spborder.get(
			`/spb/spb_planningorder/monthlyPlanningOrders/${payload}`
		);
	}
	////// INI EDIT

	getIdDetail(payload) {
		// console.log("ISI PATLOAD", payload)
		return spborder.post(`/spb/spb_planningorder/getiddetail/` + payload);
	}
	getLocation(payload) {
		return spborder.post(`/spb/spb_planningorder/getlocation/` + payload);
	}
	getTableDetailPo(payload) {
		// console.log(payload)
		return spborder.post(
			'/spb/spb_planningorder/gettabledetailpo/' + payload
		);
	}
	getPoType() {
		return spborder.post(`/spb/spb_planningorder/getpotype`);
	}
	getTerms(payload) {
		return spborder.post(
			`/spb/spb_planningorder/gettabledetailterms/` + payload
		);
	}
	getTermsSupp(payload) {
		return spborder.post(
			`/spb/spb_planningorder/gettabledetailtermssupp/` + payload
		);
	}
	getUom(payload) {
		return spborder.post(`/spb/spb_planningorder/getuom`, payload);
	}
	getAllCurrencies(payload) {
		const url = 'spb/createpo/getallcurrencies/' + payload;
		return spborder.get(url);
	}
	getFreight(payload) {
		return spborder.post(`/spb/spb_planningorder/getfreight`, payload);
	}
	getTablePoNumber() {
		return spborder.post(`/spb/spb_planningorder/gettableponumber`);
	}
	getTop(payload) {
		return spborder.post(`/spb/spb_planningorder/gettop`, payload);
	}
	getIncoterm(payload) {
		return spborder.post(`/spb/spb_planningorder/getincoterm/` + payload);
	}
	changeStatus(payload) {
		const url = '/spb/spb_planningorder/changestatus';
		return spborder.post(url, payload);
	}
	searchPopPo(payload) {
		return spborder.post('/spb/spb_planningorder/searchpoppo', payload);
	}
	notAcceptStatus(payload) {
		// console.log(payload)
		return spborder.post('/spb/spb_planningorder/notacceptstatus', payload);
	}
	saveOption(payload) {
		return spborder.post('/spb/spb_planningorder/savesubmitpo', payload);
	}
	saveOptionSupp(payload) {
		return spborder.post(
			'/spb/spb_planningorder/savesubmitposupp',
			payload
		);
	}
	getSuppBindComp(idUser) {
		return spborder.post('spb/spb_planningorder/getsuppbindcomp/' + idUser);
	}
	submitPo(payload) {
		return spborder.post('/spb/spb_planningorder/submitpo', payload);
	}
	printPlannoPur(payload) {
		return spborder.post('/spb/spb_planningorder/getprintpopur/' + payload);
	}
	printPlannoSupp(payload) {
		return spborder.post(
			'/spb/spb_planningorder/getprintposupp/' + payload
		);
	}
	confirmSupp(payload) {
		return spborder.post('/spb/spb_planningorder/confirmSupp', payload);
	}
	getRecipientMail(data) {
		return spbadmin.post(
			'/spb/notification/getPlannoRecipientMailPur',
			data
		);
	}
	sendPONotifSupp(payload) {
		const url = `spb/notification/sendPOnotifsupp`;
		return spbadmin.post(url, payload);
	}
	sendPOEmail(data) {
		// const url = `adm/send/emailPlanno`;
		// return spbKkt.post(url, data);
		const queryParams = data
			? Object.keys(data)
					.map(
						(k) =>
							encodeURIComponent(k) +
							'=' +
							encodeURIComponent(data[k])
					)
					.join('&')
			: '';
		return spbKkt.post('adm/send/emailPlannoToPurchasing?' + queryParams);
	}
	submitApi(payload) {
		return spborder.post('/spb/spb_planningorder/submitapi', payload);
	}
	// getTableGroup(params) {
	//   let queryParams = params ? Object.keys(params).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k])).join('&') : '';
	//   // console.log("payload ", queryParams);
	//   return spborder.get("/spb/spb_planningorder/getgrup?" + queryParams);
	// }
	// GetTableSearch(data) {
	//   return spborder.get("/spb/spb_planningorder/get/"+ data)
	// }
	// get(id) {
	//   return spborder.get(`/tutorials/${id}`);
	// }
	// signViaAdminSc(data) {
	//   return spborder.post("adm/auth/signviaadminsc", data);
	// }
	// update(id, data) {
	//   return spborder.put(`/tutorials/${id}`, data);
	// }
	// delete(id) {
	//   return spborder.delete(`/tutorials/${id}`);
	// }
	// deleteAll() {
	//   return spborder.delete(`/tutorials`);
	// }
	// findByTitle(title) {
	//   return spborder.get(`/tutorials?title=${title}`);
	// }
}
export default new PlanningOrder();
