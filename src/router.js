import { createRouter, createWebHashHistory } from 'vue-router';
import HomePage from './components/HomePage.vue';
import NotifDetails from './components/dashboard/NotifList.vue'
import PlannoDetail from './components/dashboard/notifdetails/PlannoDetail.vue';
import PODetail from './components/dashboard/notifdetails/PODetail.vue';
import PODetailPur from './components/dashboard/notifdetails/PODetailPur.vue';
import PlannoDetailPur from './components/dashboard/notifdetails/PlannoDetailPur.vue';
import CapaDetail from './components/dashboard/notifdetails/CapaDetail.vue';
import ShipmentDetail from './components/dashboard/notifdetails/ShipmentDetail.vue';
// import UserPriveleges from './components/user/UserPriveleges.vue'
import Guard from './app/guard/Guard';
import TestPage from './components/planningorder/PlanningOrder.vue';
import User from './components/user/UserList.vue';
import Group from './components/applicationgroup/ApplicationGroup.vue';
import GroupDetail from './components/applicationgroup/applicationgroupdetail/ApplicationGroupDetail.vue';
import CreatePo from './components/createpo/CreatePo.vue';
import SubmitPo from './components/submitpo/SubmitPo.vue';
import Pofulfillment from './components/pofulfillment/poFulfillment.vue'
import DetailPofulfillment from './components/pofulfillment/detailpofulfillment/DetailPoFulfillment.vue'
// import DetailPofulfillmentSupp from './components/pofulfillment/detailpofulfillment/DetailPofulfillmentSupp.vue'

const routes = [
	{
		path: '/',
		redirect: '/login',
	},
	{
		path: '/dashboard',
		name: 'dashboard',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: HomePage,
	},
	{
		path: '/dashboard/notifdetails',
		name: 'notifdetails',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: NotifDetails,
	},
	{
		path: '/dashboard/notifdetails/plannodetail',
		name: 'plannodetail',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: PlannoDetail,
	},
	{
        path: '/dashboard/notifdetails/plannodetailpur',
        name: 'plannodetailpur',
        beforeEnter(to, from, next) {
            Guard.guardMyroute(to, from, next);
        },
        component: PlannoDetailPur,
    },
	{
		path: '/dashboard/notifdetails/podetail',
		name: 'podetail',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: PODetail,
	},
	{
		path: '/dashboard/notifdetails/podetailpur',
		name: 'podetailpur',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: PODetailPur,
	},
	{
		path: '/dashboard/notifdetails/capadetail',
		name: 'capadetail',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: CapaDetail,
	},
	{
		path: '/dashboard/notifdetails/shipmentdetail',
		name: 'shipmentdetail',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: ShipmentDetail,
	},
	{
		path: '/mgm/user/userslist',
		name: 'user',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: User,
		children: [
			{ path: 'detail', name: 'user.detail', component: TestPage },
			{ path: 'detail#/:id', component: HomePage },
		],
	},
	{
		path: '/mgm/acl/grouplist',
		name: 'group',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: Group,

		// children: [
		//     {   path: 'detail',
		//         name: 'user.detail',
		//         component: TestPage
		//     },
		//     { path: 'detail#/:id', component : HomePage}

		// ],
	},
	{
		path: '/mgm/acl/grouplist/:id',
		name: 'groupListDetails',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: GroupDetail,
		props: true,
	},
	{
		path: '/user/:id',
		name: 'userCreate',
		// beforeEnter(to, from, next) {
		//     Guard.guardMyroute(to, from, next);
		// },

		component: () => import('./components/user/UserCreate.vue'),
	},
	{
		// path: '/mgm/acl/grouplist',
		// name: 'grouplist',
		// beforeEnter(to, from, next) {
		//     Guard.guardMyroute(to, from, next);
		// },
		// component: UserPriveleges,
		path: '/mgm/user/userslist',
		name: 'userList',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: () => import('./components/user/UserList.vue'),
	},
	{
		path: '/mgm/settings/planningorder',
		name: 'planningOrder',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},

		component: () => import('./components/planningorder/PlanningOrder.vue'),
	},
	{
		path: '/mgm/settings/planningorder/:id',
		name: 'planningOrderDetail',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: () =>
			import(
				'./components/planningorder/detailplanningorder/DetailPlanningOrder.vue'
			),
	},
	{
		path: '/mgm/settings/planningordersupp/:id',
		name: 'planningOrderDetailSupp',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: () =>
			import(
				'./components/planningorder/detailplanningorder/DetailPlanningOrderSupp.vue'
			),
	},
	{
		path: '/printposupp',
		name: 'printposupp',
		component: () => import('./pages/PrintPlanningOrderSupp.vue'),
	},
	{
		path: '/mgm/settings/createpo',
		name: 'planningorder',
		props: true,
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: CreatePo,
	},
	{
		path: '/mgm/settings/submitpo',
		name: 'SubmitPo',
		props: true,
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: SubmitPo,
	},
	{
		path: '/mgm/settings/sysparamslist',
		name: 'poFulfillment',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: Pofulfillment
	},
	{
		path: '/mgm/settings/sysparamslist/:id',
		name: 'DetailpoFulfillment',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: DetailPofulfillment
	},
	{
		path: '/mgm/settings/sysparamslistsupp/:id',
		name: 'DetailpoFulfillmentSupp',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: () => import('./components/pofulfillment/detailpofulfillment/DetailPoFulfillmentSupp.vue'),
	},
	{
		path: '/mgm/settings/lovdynamic',
		name: 'lovdynamic',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: () => import('./components/lov/LovList.vue'),
	},
	{
		path: '/mgm/settings/lovdetail',
		name: 'lovdetail',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: () => import('./components/lov/LovDetail.vue'),
	},
	{
		path: '/mgm/settings/lovedit',
		name: 'lovedit',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: () => import('./components/lov/LovEdit.vue'),
	},
	{
		path: '/formlayout',
		name: 'formlayout',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: () => import('./components/FormLayoutDemo.vue'),
	},
	{
		path: '/input',
		name: 'input',
		beforeEnter(to, from, next) {
			Guard.guardMyroute(to, from, next);
		},
		component: () => import('./components/InputDemo.vue'),
	},
	{
		path: '/floatlabel',
		name: 'floatlabel',
		component: () => import('./components/FloatLabelDemo.vue'),
	},
	{
		path: '/invalidstate',
		name: 'invalidstate',
		component: () => import('./components/InvalidStateDemo.vue'),
	},
	{
		path: '/button',
		name: 'button',
		component: () => import('./components/ButtonDemo.vue'),
	},
	{
		path: '/table',
		name: 'table',
		component: () => import('./components/TableDemo.vue'),
	},
	{
		path: '/list',
		name: 'list',
		component: () => import('./components/ListDemo.vue'),
	},
	{
		path: '/tree',
		name: 'tree',
		component: () => import('./components/TreeDemo.vue'),
	},
	{
		path: '/panel',
		name: 'panel',
		component: () => import('./components/PanelsDemo.vue'),
	},
	{
		path: '/overlay',
		name: 'overlay',
		component: () => import('./components/OverlayDemo.vue'),
	},
	{
		path: '/media',
		name: 'media',
		component: () => import('./components/MediaDemo.vue'),
	},
	{
		path: '/mgm',
		component: () => import('./components/MenuDemo.vue'),
		children: [
			{
				path: '',
				component: () => import('./components/menu/PersonalDemo.vue'),
			},
			{
				path: '/menu/seat',
				component: () => import('./components/menu/SeatDemo.vue'),
			},
			{
				path: '/menu/payment',
				component: () => import('./components/menu/PaymentDemo.vue'),
			},
			{
				path: '/menu/confirmation',
				component: () =>
					import('./components/menu/ConfirmationDemo.vue'),
			},
		],
	},
	{
		path: '/messages',
		name: 'messages',
		component: () => import('./components/MessagesDemo.vue'),
	},
	{
		path: '/file',
		name: 'file',
		component: () => import('./components/FileDemo.vue'),
	},
	{
		path: '/chart',
		name: 'chart',
		component: () => import('./components/ChartDemo.vue'),
	},
	{
		path: '/misc',
		name: 'misc',
		component: () => import('./components/MiscDemo.vue'),
	},
	{
		path: '/crud',
		name: 'crud',
		component: () => import('./pages/CrudDemo.vue'),
	},
	{
		path: '/timeline',
		name: 'timeline',
		component: () => import('./pages/TimelineDemo.vue'),
	},
	{
		path: '/empty',
		name: 'empty',
		component: () => import('./components/EmptyPage.vue'),
	},
	{
		path: '/documentation',
		name: 'documentation',
		component: () => import('./components/Documentation.vue'),
	},
	{
		path: '/blocks',
		name: 'blocks',
		component: () => import('./components/BlocksDemo.vue'),
	},
	{
		path: '/icons',
		name: 'icons',
		component: () => import('./components/IconsDemo.vue'),
	},
	{
		path: '/landing',
		name: 'landing',
		component: () => import('./pages/LandingDemo.vue'),
	},
	{
		path: '/login',
		name: 'login',
		component: () => import('./pages/Login.vue'),
	},
	{
		path: '/error',
		name: 'error',
		component: () => import('./pages/Error.vue'),
	},
	{
		path: '/notfound',
		name: 'notfound',
		component: () => import('./pages/NotFound.vue'),
	},
    {
        path: '/printpopur',
        name: 'printpopur',
        component: () => import('./pages/PrintPlanningOrderPur.vue')
    },
];

const router = createRouter({
	history: createWebHashHistory(),
	routes,
});

export default router;
