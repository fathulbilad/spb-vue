
class Guard {
    guardMyroute(to, from, next){
        // console.log('cek to.name::', to.name)
        if(to.name == 'resetpassword' || sessionStorage.authKey != null){
            next(); 
        } else {
            next('/login'); 
        }
    }
}

export default new Guard();