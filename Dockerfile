FROM node:lts-alpine

# buat folder 'app' pada direktori yang sedang dikerjakan
WORKDIR /app

# salin 'package.json' dan 'package-lock.json' (jika ada)
COPY package*.json ./

# pasang dependecy proyek
RUN npm install

# salin berkas-berkas proyek serta folder-foldernya ke direktori yang sedang dikerjakan (misal. folder 'app)
COPY . .

# bangun aplikasi untuk produksi dengan minifikasi
RUN npm run build
RUN npm install -g serve

# pasang curl
RUN apk --no-cache add curl

EXPOSE 4200
CMD [ "serve", "-s", "dist" ]